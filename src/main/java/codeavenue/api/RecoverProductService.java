package codeavenue.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.query.Query;

import codeavenue.domain.Product;
import codeavenue.util.HibernateSessionFactoryUtil;

@Path("/product/recover")
public class RecoverProductService {

	/**
	 * this should return with or without childs, with or without images
	 * 
	 * @param productId
	 * @param images
	 * @param childs
	 * @return
	 */
	@GET
	@Produces("text/plain")
	public String execute(@PathParam("productId") Long productId, @PathParam("images") Boolean images,
			@PathParam("childs") Boolean childs) {

		Session openSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
		List<Product> results = new ArrayList<Product>();

		if (productId != null) {
			Query createQuery = openSession.createQuery("From Product where id = :code ");
			createQuery.setParameter("code", productId);
			List<Product> resultList = createQuery.getResultList();
			if (!resultList.isEmpty() && resultList.size() == 1) {
				Product product = resultList.get(0);
				if (!images) {
					product.setImage(null);
				}
				if (!childs) {
					product.setProductSons(null);
				}
				results.add(product);
			} else {
				return results.toString();
			}

		} else {
			Query createQuery = openSession.createQuery("From Product");
			createQuery.setParameter("code", productId);
			List<Product> resultList = createQuery.getResultList();
			if (!resultList.isEmpty()) {
				for (Product product : resultList) {
					if (!images) {
						product.setImage(null);
					}
					if (!childs) {
						product.setProductSons(null);
					}
					results.add(product);
				}
			} else {
				return results.toString();
			}
		}

		return results.toString();
	}

	@GET
	@Produces("text/plain")
	public String getChilds(@PathParam("productId") Long productId) {
		Product product = getProduct(productId);
		if(product == null) {
			return "fail";
		}
		HibernateSessionFactoryUtil.shutdown();
		return product.getProductSons().toString();
	}
	
	@GET
	@Produces("text/plain")
	public String getImages(@PathParam("productId") Long productId) {
		Product product = getProduct(productId);
		if(product == null) {
			return "fail";
		}
		HibernateSessionFactoryUtil.shutdown();
		return product.getImage().toString();
	}

	private Product getProduct(Long productId) {
		Product product = null;
		Session openSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
		Query createQuery = openSession.createQuery("From Product where id = :code ");
		createQuery.setParameter("code", productId);
		List<Product> resultList = createQuery.getResultList();
		if (!resultList.isEmpty() && resultList.size() == 1) {
			product = resultList.get(0);
			return product;
		}
		return null;
	}

}
