package codeavenue.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Product extends BaseDomainEntity {
	
	@OneToMany(mappedBy="fatherProduct",cascade=CascadeType.PERSIST)
	private List<Product> productSons = new ArrayList<Product>();
	
	@ManyToOne
	private Product fatherProduct;
	
	@OneToMany(mappedBy="product",cascade=CascadeType.PERSIST)
	private List<Image> image = new ArrayList<Image>();


	/**
	 * @return the image
	 */
	public List<Image> getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(List<Image> image) {
		this.image = image;
	}

	/**
	 * @return the productSons
	 */
	public List<Product> getProductSons() {
		return productSons;
	}

	/**
	 * @param productSons the productSons to set
	 */
	public void setProductSons(List<Product> productSons) {
		this.productSons = productSons;
	}
	
	/**
	 * @return the fatherProduct
	 */
	public Product getFatherProduct() {
		return fatherProduct;
	}

	/**
	 * @param fatherProduct the fatherProduct to set
	 */
	public void setFatherProduct(Product fatherProduct) {
		this.fatherProduct = fatherProduct;
	}
	
}
