package codeavenue.services.crud;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.query.Query;

import codeavenue.domain.Product;
import codeavenue.util.HibernateSessionFactoryUtil;

@Path("/product/insertson")
public class ProductSonInsertService {
    
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public String execute(@PathParam("productId") Long productId, @PathParam("sonId") Long sonId) {
    	Session openSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    	Product product = null;
    	if(productId != null) {
    		Query createQuery = openSession.createQuery("From Product where id = :code ");
    		createQuery.setParameter("code", productId);
    		List<Product> resultList = createQuery.getResultList();
    		if(!resultList.isEmpty() && resultList.size() == 1) {
    			product = resultList.get(0);
    		} else {
    			return "Product not found!";
    		}
    	}
    	if(product != null) {
    		Query createQuery = openSession.createQuery("From Product where id = :code ");
    		createQuery.setParameter("code", sonId);
    		List<Product> resultList = createQuery.getResultList();
    		if(!resultList.isEmpty() && resultList.size() == 1) {
    			Product son = resultList.get(0);
				product.getProductSons().add(son);
				son.setFatherProduct(product);
				openSession.save(son);
    		} else {
    			return "Product SON not found!";
    		}
    	}
    	openSession.save(product);
    	HibernateSessionFactoryUtil.shutdown();
        return "Ok";
    }
}