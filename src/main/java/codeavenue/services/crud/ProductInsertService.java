package codeavenue.services.crud;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.query.Query;

import codeavenue.domain.Product;
import codeavenue.util.HibernateSessionFactoryUtil;

@Path("/product/insert")
public class ProductInsertService {
    
	@GET
    @Produces("text/plain")
    public String execute(@PathParam("name") String name, @PathParam("fatherId") Long fatherId, @PathParam("productId") Long productId) {
    	Product product = null;
    	Session openSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    	if(productId == null) {
    		product = new Product();
    	} else { //check if it exists, if yes, its an update
    		Query createQuery = openSession.createQuery("From Product where id = :code ");
    		createQuery.setParameter("code", fatherId);
    		List<Product> resultList = createQuery.getResultList();
    		if(!resultList.isEmpty() && resultList.size() == 1) {
    			product = resultList.get(0);
    		} else {
    			product = new Product();
    		}
    	}
    	product.setName(name);
    	
    	if(fatherId != null) {
    		Query createQuery = openSession.createQuery("From Product where id = :code ");
    		createQuery.setParameter("code", fatherId);
    		List<Product> resultList = createQuery.getResultList();
    		if(!resultList.isEmpty() && resultList.size() == 1) {
    			Product father = resultList.get(0);
				product.setFatherProduct(father);
				father.getProductSons().add(product);
				openSession.save(father);
    		}
    	}
    	openSession.save(product);
    	HibernateSessionFactoryUtil.shutdown();
        return "Ok";
    }
}