package codeavenue.services.crud;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import codeavenue.domain.Image;

// The Java class will be hosted at the URI path "/helloworld"
@Path("/image")
public class ImageInsertService {
    
    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public String execute(@PathParam("name") String name, @PathParam("productId") Long id) {
    	Image image = new Image();
        return "OK";
    }
}